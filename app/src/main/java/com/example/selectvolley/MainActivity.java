package com.example.selectvolley;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

//import static com.android.volley.VolleyLog.TAG;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String URL_SELECT = "http://192.168.1.3/Crudvolley/select.php";
    public static final String URL_DELETE = "http://192.168.1.3/Crudvolley/delete.php";
    public static final String URL_EDIT = "http://192.168.1.3/Crudvolley/edit.php";
    public static final String URL_INSERT = "http://192.168.1.3/Crudvolley/insert.php";
    ListView list;
    SwipeRefreshLayout swipe;
    List<Data> itemList = new ArrayList<Data>();
    MhsAdapter adapter;
    LayoutInflater inflater;
    EditText tid, tnim, tnama, talamat;
    String vid,vnim,vnama,valamat;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fab = findViewById(R.id.fab);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        list = (ListView) findViewById(R.id.list);

        adapter = new MhsAdapter(MainActivity.this, itemList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(true);
                itemList.clear();
                adapter.notifyDataSetChanged();
                callVolley();
            }

        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //statement jika fab diklik
                DialogForm("","","","","tambah");
            }
        });


        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

                final String idx = itemList.get(position).getId();
                final CharSequence pilihanAksi[] ={"Hapus","Ubah"};
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                dialog.setItems(pilihanAksi, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        switch (which)
                        {
                            case 0 :
                                //jika dihapus
                                hapusData(idx);
                                break;
                            case 1:
                                ubahData(idx);
                                //jika edit
                        }
                    }
                }).show();
                return false;
            }
        });
    }

    @Override
    public void onRefresh(){
        itemList.clear();
        adapter.notifyDataSetChanged();
        callVolley();
    }

    private void callVolley(){
        itemList.clear();
        adapter.notifyDataSetChanged();
        swipe.setRefreshing(true);

        // membuat request JSON

        JsonArrayRequest jArr = new JsonArrayRequest (URL_SELECT, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //parsing json
                for (int i = 0; i < response.length();i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        Data item = new Data();

                        item.setId(obj.getString("id"));
                        item.setNim(obj.getString("nim"));
                        item.setNama(obj.getString("nama"));
                        item.setAlamat(obj.getString("alamat"));

                        //menambah item ke array
                        itemList.add(item);

                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                //notifikasi adanya perubahan adapter
                adapter.notifyDataSetChanged();

                swipe.setRefreshing(false);
                }

            },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(TAG,"ERROR : "+ error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // menambah request ke request queque
        RequestQueue mRequestQueque = Volley.newRequestQueue(getApplicationContext());
        mRequestQueque.add(jArr);

    }

    public void hapusData(String id){

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DELETE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                            callVolley();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this,"gagal koneksi ke server, tolong cek kembali konksi anda",Toast.LENGTH_LONG).show();
                }
            })

            {
                @Override
                protected Map<String,String> getParams() throws AuthFailureError {
                    //posting parameters he post url
                    Map<String, String> params = new HashMap<>();

                    params.put("id", id);
                    return params;

                }
            };
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            queue.add(stringRequest);
        }


        public void ubahData(String id){

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_EDIT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jObj = new JSONObject(response);

                                String idx = jObj.getString("id");
                                String nimx = jObj.getString("nim");
                                String namax = jObj.getString("nama");
                                String alamatx = jObj.getString("alamat");
                                DialogForm(idx,nimx, namax, alamatx, "update");



                                adapter.notifyDataSetChanged();
                                callVolley();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this,"gagal koneksi ke server, tolong cek kembali konksi anda",Toast.LENGTH_LONG).show();
                }
            })

            {
                @Override
                protected Map<String,String> getParams() throws AuthFailureError {
                    //posting parameters ke post url
                    Map<String, String> params = new HashMap<>();

                    params.put("id", id);
                    return params;

                }
            };
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            queue.add(stringRequest);

        }

        public  void DialogForm(String id,String nim,String nama, String alamat, String btn){


            View viewDialog;

            AlertDialog.Builder dialogForm = new AlertDialog.Builder(MainActivity.this);
            inflater = getLayoutInflater();
            viewDialog = inflater.inflate(R.layout.form_mahasiswa,null);
            dialogForm.setView(viewDialog);
            dialogForm.setCancelable(true);
            dialogForm.setTitle("Form Mahasiswa");

            tid =(EditText) viewDialog.findViewById(R.id.inId);
            tnim =(EditText) viewDialog.findViewById(R.id.inNim);
            tnama =(EditText) viewDialog.findViewById(R.id.inNama);
            talamat =(EditText) viewDialog.findViewById(R.id.inAlamat);

            if (id.isEmpty()){
                tid.setText(null);
                tnim.setText(null);
                tnama.setText(null);
                talamat.setText(null);
            }
            else {
                tid.setText(id);
                tnim.setText(nim);
                tnama.setText(nama);
                talamat.setText(alamat);
            }

            dialogForm.setPositiveButton(btn, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    vid = tid.getText().toString();
                    vnim = tnim.getText().toString();
                    vnama = tnama.getText().toString();
                    valamat = talamat.getText().toString();

                    simpan();
                    dialogInterface.dismiss();
                    callVolley();
                }
            });

            dialogForm.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    tid.setText(null);
                    tnim.setText(null);
                    tnama.setText(null);
                    talamat.setText(null);
                }
            });

            dialogForm.show();

        }

        public  void simpan(){
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_INSERT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(MainActivity.this,response,Toast.LENGTH_LONG).show();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this,"Gagal koneksi ke server, cek koneksi anda",Toast.LENGTH_LONG).show();
                }
            })

            {
                @Override
                protected Map<String,String> getParams() throws AuthFailureError {
                    //posting parameters he post url
                    Map<String, String> params = new HashMap<>();

                    Log.i(TAG, "id: "+vid);
                    Log.i(TAG, "nim: "+vnim);
                    Log.i(TAG, "nama: "+vnama);
                    Log.i(TAG, "alamat: "+valamat);

                    if (vid.isEmpty()){
                        params.put("nim", vnim);
                        params.put("nama", vnama);
                        params.put("alamat", valamat);
                        return params;
                    }
                    else{
                        params.put("id", vid);
                        params.put("nim", vnim);
                        params.put("nama", vnama);
                        params.put("alamat", valamat);
                        return params;
                    }
                }
            };
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            queue.add(stringRequest);
        }
}